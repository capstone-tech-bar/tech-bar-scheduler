# Tech Bar Scheduler
### University of Arkanasas Capstone Spring 2022 Team #9
##### System dependencies
> - Ruby version ~> `3.1.1`
> - Rails version ~> `7.0.2.2`
> - PostgreSQL version ~> `14.2`
> - NodeJS
##### Configuration 
> - Download the project files, then run `bundle install` in the local directory to auto-install resources for the project
##### Database creation
> - `rake db:create`
##### Database initialization
> - `rake db:migrate`
> - `rake db:seed`
##### How to launch the project
> - `rails s`


