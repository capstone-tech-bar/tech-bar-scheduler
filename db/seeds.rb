# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
bar_list = [
  [ "Otis Corley", "2501 SE Otis Corley Dr", "Bentonville", "AR", "USA", "72712", "Open", 9, 17, 0],
  [ "Walmart Home Office", "702 SW 8th St", "Bentonville", "AR", "USA", "72712", "Open", 9, 17, 0],
  [ "Walmart DGTC West", "805 Moberly Ln", "Bentonville", "AR", "USA", "72712", "Open", 9, 17, 0],
  [ "Hog Wild", "2908 SE I St", "Bentonville", "AR", "USA", "72712", "Open", 9, 17, 0],
  [ "Walmart J Street Office", "2608 SE J St", "Bentonville", "AR", "USA", "72712", "Open", 9, 17, 0],
  ["Walmart Labs", "860 W California Ave", "Sunnyvale", "CA", "USA", "94086", "Open", 9, 17, 0],
  ["Walmart eCommerce Corporate", "850 Cherry Ave", "San Bruno", "CA", "USA", "94066", "Open", 9, 17, 0],
  ["Hoboken Office", "221 River St", "Hoboken", "NJ", "USA", "07030", "Open", 9, 17, 0],
  ["Walmart Labs", "10780 Parkridge Blvd #70", "Reston", "VA", "USA", "20191", "Open", 9, 17, 0]
]

bar_list.each do |name, address, city, state, country, zip, status, openTime, closeTime, size|
  Bar.create(name: name, address: address, city: city, state: state, country: country, zip: zip, status: status, openTime: openTime, closeTime: closeTime, size: size)
end

User.create(:username=> "admin", :password=> "password", :admin=> true)