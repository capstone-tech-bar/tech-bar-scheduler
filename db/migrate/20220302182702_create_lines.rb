class CreateLines < ActiveRecord::Migration[7.0]
  def change
    create_table :lines do |t|
      t.column :bar_id, :integer, :null => false
      t.column :spot, :integer, :null => false
      t.column :user_id, :integer, :null => false
      t.column :name, :string, :null => false
      t.column :device, :string, :null => false
      t.column :issue, :text, :null => false
      t.timestamps
    end
  end
end
