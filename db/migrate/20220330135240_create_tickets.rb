class CreateTickets < ActiveRecord::Migration[7.0]
  def change
    create_table :tickets do |t|
      t.column :bar_id, :integer, :null => false
      t.column :user_id, :integer, :null => false
      t.column :name, :string, :null => false
      t.column :device, :string, :null => false
      t.column :issue, :text, :null => false
      t.column :status, :string, :null => false
      t.column :techNote, :text
      t.column :dateResolved, :date
      t.timestamps
    end
  end
end
