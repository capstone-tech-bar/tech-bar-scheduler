class CreateBars < ActiveRecord::Migration[7.0]
  def change
    create_table :bars do |t|
      t.column :name, :string, :null => false
      t.column :address, :string, :null => false
      t.column :city, :string, :null => false
      t.column :state, :string, :limit => 2, :null => false
      t.column :country, :string, :null => false
      t.column :zip, :string, :limit => 5, :null => false
      t.column :status, :string, :null => false
      t.column :openTime, :integer, :null => false
      t.column :closeTime, :integer, :null => false
      t.column :size, :integer, :null => false
      t.column :latitude, :float
      t.column :longitude, :float
      t.timestamps
    end
  end
end
