import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
	static targets = ["link"]

	connect() {
    // this.element.textContent = "Hello From geo_location_controller!"
	// this.geolocate()
 	}
	geolocate(){
		if (!navigator.geolocation) {
			this.linkTarget.textContent = "Geolocation not supported in this browser"
		} else {
			navigator.geolocation.getCurrentPosition(this.success.bind(this), this.error.bind(this))
			// let options = {
			// 	enableHighAccuracy: false,
			// 	timeout: 5000,
			// 	maximumAge: 0
			// }
			// navigator.geolocation.watchPosition(this.success.bind(this), this.error.bind(this))
		}
		}




	success(posistion){
		this.linkTarget.textContent = `Latitude: ${posistion.coords.latitude}, Longitude: ${posistion.coords.longitude}`
	}
	error(){
		this.linkTarget.textContent = "unable to get location"
	}







}
