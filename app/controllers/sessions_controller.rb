class SessionsController < ApplicationController

	def create
        @user = User.find_by(username: params[:username])
        if @user && @user.authenticate(params[:password])
            session[:user_id] = @user.id
            # redirect_to @user
            if @user.admin == true
                redirect_to techpage_path
            else
                redirect_to bars_path
            end
        else
            flash[:danger] = "Something went wrong, make sure the username and password is correct."
            redirect_to login_path
        end
    end

    def destroy
        session.delete(:user_id)
        session[:user_id] = nil

        flash[:info] = "You have been signed out."
        redirect_to root_path
    end


end