class LineController < ApplicationController
    def line_params
        params.require(:lines).permit(:bar_id, :spot)
    end
end
