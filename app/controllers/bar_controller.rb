class BarController < ApplicationController
    def list
        @bars = Bar.all
        @user_long = request.location.longitude
        @user_lat = request.location.latitude
    end
    
    def show
        if Line.where(id: params[:id]).count > 0
            @line = Line.find(params[:id])
            @bar = Bar.find(@line.bar_id)
            if @bar.openTime >= 13
                @open = (@bar.openTime - 12).to_s + "PM"
            elsif @bar.openTime == 12
                @open = "12 PM"
            else
                @open = @bar.openTime.to_s + "AM"
            end
            if @bar.closeTime >= 13
                @close = (@bar.closeTime - 12).to_s + "PM"
            elsif @bar.closeTime == 12
                @close = "12 PM"
            else
                @close = @bar.closeTime.to_s + "AM"
            end
                
            if @line.spot == 1
                @grammar1 = "is"
                @grammar2 = "person"
            else
                @grammar1 = "are"
                @grammar2 = "people"
            end
        else
            redirect_to :action => 'list'
        end
    end
    
    def createSpot
        if Line.where(user_id: line_params[:user_id]).count == 0
            @line = Line.new(line_params)
            @line.bar_id ||= params[:id]
            @line.user_id ||= session[:user_id]
            @line.spot ||= Line.where(bar_id: params[:id]).count
            if @line.save
                @bar = Bar.find(params[:id])
                @bar.update_attribute(:size, @bar.size + 1)
                redirect_to :action => 'show', :id => @line.id, :message => "Welcome!"
            else
                redirect_to :action => 'enterQueue'
            end
        else
            @line = Line.find_by(user_id: line_params[:user_id])
            redirect_to :action => 'show', :id => @line.id, :message => "Please Leave this Queue if you would like to enter another!"
        end
    end 

    def enterQueue
        @line = Line.new
    end

    def line_params
        params.require(:lines).permit(:user_id, :name, :device, :issue)
    end

    def new
        @bar = Bar.new
    end
    
    def create
        @bar = Bar.new(bar_params)
        @bar.size ||= '0'

        if @bar.save
            redirect_to :action => 'list'
        else
            render :action => 'new'
        end
    end

    def bar_params
        params.require(:bars).permit(:name, :address, :city, :state, :zip, :status, :size)
    end
    
    def edit
    end
    
    def update
        @lines = Line.where(bar_id: params[:bar_id])
        for i in @lines do
            if i.spot > params[:spot].to_i
                i.update_attribute(:spot, i.spot - 1)
            end
        end
        redirect_to :action => 'list'
    end

    def leave
        if Line.where(id: params[:id]).count > 0
            if Line.find(params[:id]).destroy
                @bar = Bar.find(params[:bar_id])
                @bar.update_attribute(:size, @bar.size - 1)
            end
            redirect_to :action => 'update', :bar_id => params[:bar_id], :spot => params[:spot]
        else
            redirect_to :action => 'list'
        end
    end
    
    def delete
    end
    def CheckTickets
        @tickets = Line.all
    end
end
