class TechpageController < ApplicationController
    before_action :require_admin

    def index
        @bar = Bar.find(params[:id])
        @tickets = Ticket.where(bar_id: params[:id], status: "Open")
        if @bar.openTime >= 13
            @open = (@bar.openTime - 12).to_s + "PM"
        elsif @bar.openTime == 12
            @open = "12 PM"
        else
            @open = @bar.openTime.to_s + "AM"
        end
        if @bar.closeTime >= 13
            @close = (@bar.closeTime - 12).to_s + "PM"
        elsif @bar.closeTime == 12
            @close = "12 PM"
        else
            @close = @bar.closeTime.to_s + "AM"
        end
    end

    def closeTicket
        @tickets = Ticket.where(bar_id: params[:id], status: "Open")
    end

    def close
        @ticket = Ticket.find(params[:id])
        @ticket.update_attribute(:status, "Closed")
        @ticket.update_attribute(:dateResolved, Time.now)
        redirect_to :action => 'index', id: @ticket.bar_id
    end
    def openTicket
        @lines = Line.where(bar_id: params[:id])
    end
    def createTicket
        @line = Line.find(params[:id])
        @bar = Bar.find(@line.bar_id)
        @ticket = Ticket.new
        @ticket.bar_id ||= @line.bar_id
        @ticket.user_id ||= @line.user_id
        @ticket.name ||= @line.name
        @ticket.device ||= @line.device
        @ticket.issue ||= @line.issue
        @ticket.status||= 'Open'
        if @ticket.save
            if @line.destroy
                @lines = Line.where(bar_id: @bar.id)
                for i in @lines do
                    if i.spot > params[:spot].to_i
                        i.update_attribute(:spot, i.spot - 1)
                    end
                end
                @bar.update_attribute(:size, @bar.size - 1)
                redirect_to :action => 'index', id: @bar.id
            else
                redirect_to :action => 'openTicket'
            end
        else
            redirect_to :action => 'openTicket'
        end
    end
    def chooseBar
        @bars = Bar.all
    end

    def changeStatus
        @bar = Bar.find(params[:id])
        if @bar.status == "Open"
            @bar.update_attribute(:status, "Closed")
        else
            @bar.update_attribute(:status, "Open")
        end
        redirect_to :action => 'index', id: @bar.id
    end

    def changeHours
        @bar = Bar.find(params[:id])
        if params[:time] == "Open" && params[:direction] == "+"
            if @bar.openTime + 1 < @bar.closeTime
                @bar.update_attribute(:openTime, @bar.openTime + 1)
            end
        elsif params[:time] == "Open" && params[:direction] == "-"
            if @bar.openTime - 1 > 6
                @bar.update_attribute(:openTime, @bar.openTime - 1)
            end
        elsif params[:time] == "Close" && params[:direction] == "+"
            if @bar.closeTime + 1 < 21
                @bar.update_attribute(:closeTime, @bar.closeTime + 1)
            end
        else
            if @bar.closeTime - 1 > @bar.openTime
                @bar.update_attribute(:closeTime, @bar.closeTime - 1)
            end
        end
        redirect_to :action => 'index', id: @bar.id
    end

    def require_admin
        if !is_admin?
            flash[:danger] = "You must be a Technician to access this page!"
            redirect_to bars_path
        end
    end
end