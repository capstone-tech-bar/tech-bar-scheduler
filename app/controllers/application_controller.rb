class ApplicationController < ActionController::Base
    before_action :require_user, :except => [:new, :create, :testlogin]
    before_action :is_logged_in?, :only => [:new, :create, :testlogin]
    add_flash_types :info, :error, :warning, :danger
    helper_method :current_user, :logged_in?, :is_admin?
    def home
    end

    def index
        flash.notice = 'No page found at that address'
        redirect_to root_path
    end

    def logged_in?
        session[:user_id]
    end

    def current_user
        @current_user ||= User.find_by_id(session[:user_id]) if !! session[:user_id]
    end

    def is_admin?
        current_user.admin
    end

    private

    def require_user
        if !logged_in?
            flash[:danger] = "You must be logged in to access the website!"
            redirect_to root_path
        end
    end

    def is_logged_in?
        if logged_in?
            flash[:danger] = "You're already logged in."
            if is_admin?
                redirect_to techpage_path
            else
                redirect_to bars_path
            end
        end
    end
end
