class UsersController < ApplicationController
    
    def new
        @user = User.new
    end

    def create
        @user = User.new(user_params)
        if @user.save
            session[:user_id] = @user_id
            flash[:success] = "Your account has been created."
            redirect_to login_path
        else
            flash[:danger] = "Something went wrong, Please try again."
            redirect_to signup_path
        end
    end

    def show
        @user = User.find(params[:id])
    end

    private

    def user_params
        params.require(:user).permit(:username, :password)
    end
end