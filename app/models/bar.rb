class Bar < ApplicationRecord
    has_many :lines
    validates_presence_of :name
    def Location
        [address, city, state, country].compact.join(', ')
    end
    geocoded_by :Location
    after_validation :geocode
end
