Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "sessions#new"



  # Users Routes
  resources :users, only: [:new, :create, :edit, :update, :show, :destroy]

  # User Sessions Routes
  get "login" , to:"sessions#new"
  post "login" , to:"sessions#create"
  get "logout", to:"sessions#destroy"
  post "logout", to: "essions#destroy"
  delete "logout", to:"sessions#destroy"
  get "signup", to:"users#new"
#   get "showusers", to:"users#show"

  get "bars", to:"bar#list"

  get "/inqueue" , to:"inqueue#index"
  get "/techpage", to: "techpage#chooseBar"
  get 'bar/list'
  get 'bar/show'
  post 'bar/create'
  get 'bar/enterQueue'
  post 'bar/createSpot'
  patch 'bar/update'
  get 'bar/leave'
  get 'bar/update'
  get 'bar/CheckTickets'
  get 'techpage/index'
  get 'techpage/openTicket'
  post 'techpage/createTicket'
  get 'techpage/createTicket'
  get 'techpage/chooseBar'
  get 'techpage/closeTicket'
  get 'techpage/changeStatus'
  patch 'techpage/changeStatus'
  get 'techpage/close'
  get 'techpage/changeHours'
  get '*all', to: 'sessions#new', constraints: lambda { |req|
    req.path.exclude? 'rails/active_storage'}

end
